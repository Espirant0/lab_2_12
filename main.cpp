#include <iostream>
#include <memory>
#include <vector>
#include <string>

class Teacher;
class Student {
public:

    Student(const std::string& name)  {
        m_student_name = name;
        std::cout << m_student_name << " created" << std::endl;
    }
    ~Student() {
        std::cout<< m_student_name << " destroyed" << std::endl;
    }

    const std::string& GetStudentName() const { 
        return m_student_name; 
    }

    const std::weak_ptr<Teacher> GetTeacher() { 
        return m_teacher; 
    }

    friend void AddStudents(std::shared_ptr<Teacher>& Teacher, std::shared_ptr<Student>& Student);

private:
    std::string m_student_name;
    std::weak_ptr<Teacher> m_teacher;
};

class Teacher {
public:
    Teacher(const std::string& name) {
        m_teacher_name = name;
        std::cout << "Teacher " << m_teacher_name << " created" << std::endl;
    }
    ~Teacher() {
       std::cout << "Teacher " << m_teacher_name << " destroyed" << std::endl;
    }
    void AddStudent(std::shared_ptr<Student>& Student) {
        m_students.push_back(Student);
    }

    void ShowStudentsList() {
        std::cout << " " << std::endl;
        std::cout << "Students list of "<< m_teacher_name<< ":" << std::endl;
        for (auto const& student : m_students)
             std::cout << student->GetStudentName() << std::endl; 
        std::cout << " " << std::endl;
    }

    const std::string& GetTeacherName() const {
        return m_teacher_name; 
    }

    friend void AddStudents(std::shared_ptr<Teacher>& Teacher, std::shared_ptr<Student>& Student);

private:
    std::string m_teacher_name;
    std::vector<std::shared_ptr<Student>> m_students;

};

void AddStudents(std::shared_ptr<Teacher>& Teacher, std::shared_ptr<Student>& Student) {
    Student->m_teacher = Teacher;
    Teacher->AddStudent(Student);
}

int main() {
    auto Teacher_Tom{std::make_shared<Teacher>("Teacher_Tom") };
    auto Teacher_John{ std::make_shared<Teacher>("Teacher_John") };
    auto Student_Max{std::make_shared<Student>("Student_Max") };
    auto Student_Lilly{std::make_shared<Student>("Student_Lilly") };
    auto Student_Emily{ std::make_shared<Student>("Student_Emily") };
    
    AddStudents(Teacher_Tom, Student_Max);
    AddStudents(Teacher_Tom, Student_Lilly);
    AddStudents(Teacher_John, Student_Emily);
    
    Teacher_John->ShowStudentsList();
    Teacher_Tom->ShowStudentsList();

    return 0;
}